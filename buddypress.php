<?php
/**
Template Name: UM Template
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-content' ) ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<section>
	<div class="container">
		<div class="row x-center">

			<div class="column text-column col-10">

				<?php the_content(); ?>

				<?php endwhile; endif; ?>

				<!--<h3>Your Lessons</h3>

				 <?php if ( is_user_logged_in() ):

				    global $current_user;
				    wp_get_current_user();
				    $author_query = array(	'posts_per_page' => '-1',
				    						'post_type' => 'masterclass',
				    						'author' => $current_user->ID);
				    $author_posts = new WP_Query($author_query);
				    while($author_posts->have_posts()) : $author_posts->the_post();
				    ?>
				        <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>   </li>    
				    <?php           
				    endwhile;

				else :

				    echo "not logged in";

				endif; ?> -->

			</div>

			<div class="column col-2 sidebar">

				
				<?php  if ( is_user_logged_in() ) { ?>
				<a href="/create-a-lesson" class="button_alt">New Version <i class="fas fa-plus-circle"></i></a>
				<a href="/groups" class="button_alt">Groups <i class="fas fa-chalkboard-teacher"></i></a>
				<?php } ?>

				<?php echo do_shortcode('[ultimatemember form_id="443"]'); ?>
			</div>

		</div>
	</div>
</section>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>