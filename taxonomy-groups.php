<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section>
	<div class="container">
		<div class="row">
	
			<div class="column col-8 blog">
				<!-- <h1><?php $tax_term = $wp_query->query_vars['tax_name']; ?></h1>	 -->
				<h1>Group: <i class="fas fa-chalkboard-teacher"></i> <?php single_term_title(); ?></h1>
			</div>
		</div>
		
			<div class="row">
	
				<div class="column col-10 blog">
					
					<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
					$term = $term->slug; // will show the name
					$master_loop = new WP_Query( 
						array( 
							'post_type' => 'masterclass', 
							'groups' => '"' . $term . '"',
							'posts_per_page' => -1, 
							'orderby' => 'date',
							'order' => 'DESC',
							'meta_query' => array(
								array(
									'relation' => 'OR',
									array(
										// Will pull old masterclasses
										'key' => 'parent_version', 
										'compare' => 'NOT EXISTS'
									),
									array(
										// New masterclasses have an empty parent version if one is not set
										'key' => 'parent_version', 
										'value'   => '',
										'compare' => '=',
									)
								)
							)
						 ) 
					); ?>
					
					<?php while ( $master_loop->have_posts() ) : $master_loop->the_post(); ?>
					
						<fieldset>
	
							<div class="row">
		
								<div class="column col-6"><h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2></div>
								<div class="column col-6 text-right calendar"><i class="fas fa-calendar-alt"></i> <strong><?php echo get_the_date(); ?></strong> by <?php the_author(); ?></div>
							</div>
		
							<?php $summary = get_field('brief_lesson_summary'); ?>
		
							<div class="course_details"><?php echo $summary; ?></div>
		
							<div class="row">
		
								<div class="column col-6"><small>This lesson currently has <strong><?php comments_number( 'no responses', 'one response', '% responses' ); ?></strong></small>.</div>
								<div class="column col-6 text-right"><p><a href="<?php the_permalink(); ?>" class="btn">Go to Lesson</a></p></div>
							</div>
		
						</fieldset>
			
			
					<?php endwhile; ?>
	
				</div>
	
				<div class="column col-2 sidebar">
					
					<a href="/create-a-lesson" class="button_alt">New Version <i class="fas fa-plus-circle"></i></a>
	
					<?php echo do_shortcode('[ultimatemember form_id="443"]'); ?>
	
				</div>
	
			</div>
		
	</div>
</section>


<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>