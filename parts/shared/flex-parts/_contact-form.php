<section class="contact-form-block">
	<div class="container">

		<div class="module">
			<div class="form-container text-column">
				<h3 class="headline">Contact Us</h3>
				<!-- <h2>Get Involved</h2>
				<p>Interested in becoming connected with the UCLA Curtis Center? Be the first to hear about volunteer and career opportunities, events, and teacher resources.</p> -->

				<?php the_sub_field('intro_copy'); ?>

				<?php echo do_shortcode('[ninja_form id=1]'); ?>
				<!-- <form>
					<label class="full-row">Enter your name.</label>
					<input id="fname" type="text" name="fname" placeholder="First name..." class="half-row">
					<input id="lname" type="text" name="fname" placeholder="Last name..." class="half-row">
					<label class="full-row">Enter your email address.</label>
					<input id="email" type="email" name="email" placeholder="Email..." class="full-row">
					<label class="full-row">Enter your message.</label>
					<textarea placeholder="Message..." class="full-row" rows="4" cols="50"></textarea>
					<button class="btn">Submit</button>
				</form> -->
			</div>
			<div class="image-container"></div>
		</div>
	</div>
</section>