<section class="logo-grid">
	<div class="container">
		<h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
		<div class="logo-list">
			<?php while ( have_rows("logos") ) : the_row(); ?>
				<?php 
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$link = get_sub_field('link');
				?>
				<?php if ($link): ?>
					<a href="<?php echo $link; ?>">
				<?php endif; ?>
				
				<p><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></p>
				<h4><?php echo $title; ?></h4>
				
				<?php if ($link): ?>
					</a>
				<?php endif; ?>
			<?php endwhile ?>
		</div>
	</div>
</section>