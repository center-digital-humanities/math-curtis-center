<section class="resources-block">
	<div class="resources">
		<div class="container">
			<h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
			<div class="resource-list">
				<?php while ( have_rows("resources") ) : the_row(); ?>
					<?php 
						$title = get_sub_field('title');
						$photo = get_sub_field('photo');
						$text = get_sub_field('text');
						$link = get_sub_field('link');
						$title_color = get_sub_field('title_color');
					?>
					<div class="resource-item">
						<div class="module">
							<h3 class="headline"><span style="background-color: <?php echo $title_color; ?> ;"></span><?php echo $title; ?></h3>
							
							<?php //if ($photo): ?>
							<!-- <div class="resource-photo">
								<img src="<?php //echo $photo['url']; ?>" alt="<?php //cho $photo['alt']; ?>" />
							</div> -->
							<?php endif; ?>
							
							<div class="resource-copy">					
								<div><?php echo $text; ?></div>
							</div>
							<a href="<?php echo $link; ?>">Read More <span>&rarr;</span></a>
						</div>
					</div>
				<?php endwhile ?>
			</div>
			<div class="text-center"><a href="#" class="btn">See All</a></div>
			
		</div>
	</div>
</section>