<section class="team_members-block <?php the_sub_field('section_color_theme'); ?>">
    <div class="team-members">
        <div class="container">
            <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
            <div class="member-list">
                <?php while ( have_rows("team_members") ) : the_row(); ?>
                    <?php 
                        $photo = get_sub_field('photo');
                        $name = get_sub_field('name');
                        $title = get_sub_field('title');
                        $bio = get_sub_field('bio');
                        $linkedin = get_sub_field('linkedin');
                    ?>
                    <div class="team-member">
                        <div class="module">
                            <div class="member-photo">
                                <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
                            </div>
                            <div class="member-copy">
                                <div class="member-name"><?php echo $name; ?></div>
                                <div class="member-title"><?php echo $title; ?></div>                            
                                <div class="member-bio"><?php //echo $bio; ?></div>

                                <div id="<?php echo preg_replace('/\W+/','',strtolower(strip_tags($name))); ?>" class="lity-hide">
                                	<h1><?php echo $name; ?></h1>
                                	<h3><?php echo $title; ?></h3>
                                    <?php echo $bio; ?>
                                </div>
                                <a href="#<?php echo preg_replace('/\W+/','',strtolower(strip_tags($name))); ?>" data-lity>View Bio</a>


                            </div>
                        </div>
                    </div>
                <?php endwhile ?>
            </div>
        </div>
    </div>
</section>