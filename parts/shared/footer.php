</main>
<footer>
	<div class="footer-main">
		<div class="container">
			<div class="module">
				<div class="row">
					<div class="column brand">
						<a href="/" title="<?php bloginfo( 'name' ); ?>">
							<img src="<?php the_field('logo_contrast', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
						</a>
					</div>

					<? if( have_rows('footer_nav', 'option') ): ?>
						<? while( have_rows('footer_nav', 'option') ): the_row(); ?>

						<div class="column col-2">

							<div class="nav-title"><strong><?php echo the_sub_field('column_title'); ?></strong></div>
							<? if( have_rows('footer_column') ): ?>
								<ul>
									<? while( have_rows('footer_column') ): the_row(); ?>
										<li><a href="<? the_sub_field('url'); ?>"><? the_sub_field('text'); ?></a></li>
									<? endwhile ?>
								</ul>
							<? endif ?> 
						</div>

						<? endwhile ?>
					<? endif ?>

					<?php if( have_rows('social_media_accounts', 'option') ): ?>
					<div class="column social">
						    <ul>
						    <?php while( have_rows('social_media_accounts', 'option') ): the_row(); ?>
						    	<?php  
						    		$url = get_sub_field('url');
						    		$name = get_sub_field('name');
						    		$img = get_sub_field('icon');
						    	?>
						        <li>
						        	<a href="<?php echo $url; ?>" title="<?php echo $name; ?>">
						        		<img src="<?php echo $img; ?>" alt="<?php echo $name; ?>">
						        	</a>
						        </li>
						    <?php endwhile; ?>
						    </ul>
					</div>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
<!-- 	<div class="footer-sub">
		<div class="container">
			<div class="module">
				<div class="row x-center">
					<div class="column col-3">
						<p>© Copyright 2018 by Yantriks</p>
					</div>
					<div class="column col-4">
						<ul>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">Privacy Statement</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> -->
</footer>