<div class="header">
	<header class="header-mobile">
		<div class="container">
			<div class="module">
				<div class="brand">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
					</a>
				</div>
				<div class="navburger">
					<div class="buns">
						<span class="patty"></span>
						<span class="patty"></span>
						<span class="patty"></span>
					</div>
				</div>
			</div>
		</div>
	</header>
<div class="header-wrapper">
	<div class="menu-close"></div>
	<div class="super-header" id="super-header">
		<div class="container">
			<div class="module">
				<div class="super-nav">
				<div class="nav-main">
					<nav>
						<?php wp_nav_menu( array( 'theme_location' => 'super-nav') ); ?>
					</nav>
				</div>
				<div class="search-form-wrap"><?php get_search_form(); ?></div>
				</div>
			</div>
		</div>
	</div>
	<header class="header-main">
		<div class="container">
			<div class="module">
				<div class="brand">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
					</a>
				</div>
				<div class="sub-pages-nav">
					<nav class="large-only">
						<?php wp_nav_menu( array( 'theme_location' => 'main-menu') ); ?>
					</nav>
					<nav class="small-only">
						<?php wp_nav_menu( array( 'theme_location' => 'mobile-nav') ); ?>
					</nav>
				</div>
			</div>
		</div>
	</header>
</div>
</div>
<main>
