$(document).ready(function() {

    var viewPort = {
        width: $(window).width(),
        height: $(window).height(),
    };

    // Mobile Nav Events
    $('.header-mobile .navburger').on('click', function() {
        $('.header-wrapper').toggleClass('nav-open');
        $('body').css({ 'position': 'fixed' });
    });

    $('.menu-close').on('click', function() {
        $('.header-wrapper').toggleClass('nav-open');
        $('body').css({ 'position': 'relative' });
    });

    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 50) {
            $(".header").addClass("active");
        } else {
            $(".header").removeClass("active");
        }
    });

    if (!!$('.hero-slides')) {
        $('.hero-slides').slick({
            dots: true,
            infinite: true,
            speed: 400,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 10000
        });
    }

    if (!!$('.two-col-slides')) {
        $('.two-col-slides').slick({
            dots: true,
            infinite: true,
            speed: 400,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            arrows: false,
            autoplaySpeed: 10000,
            adaptiveHeight: true
        });
    }

    if (!!$('.tabs-content')) {
        $('.tabs-content').slick({
            arrows: false,
            fade: true,
            speed: 400,
            dots: false,
            responsive: [
            {
              breakpoint: 939,
              settings: {
                fade: false,
                arrows: false,
                centerMode: true,
                centerPadding: '35px',
                slidesToShow: 1
              }
            }
            ]
        });

        $('.tabs-nav ul li:first-child').addClass('current');

        $('.tabs-nav ul li').click(function() {
            var slideNumber = $(this).data('slide');
            $('.tabs-content').slick('slickGoTo', slideNumber - 1);
            $('.tabs-nav ul li').removeClass('current');
            $(this).addClass('current');
        });
    }

    if (!!$('.logos-slides')) {
        $('.logos-slides').slick({
            fade: true,
            arrows: false,
            dots: false,
            infinite: false,
            autoplay: true,
            autoplaySpeed: 5000,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    }

    $("#open-edit-modal").click(function(e){
        e.preventDefault();
        $("#edit-lesson-modal").addClass('visible');
    });
    $("#close-button").click(function(){
        $("#edit-lesson-modal").removeClass('visible');
    });

});