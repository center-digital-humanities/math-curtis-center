<?php
/**
 * The Template for displaying all single posts
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<section class="single-column-block <?php if($noPaddingBottom){ ?> no-padding-bottom <?php } ?>">
		<div class="container">
			<div class="row x-center">

				<div class="column col-8 blog">

					<h2><?php the_title(); ?></h2>

					<?php if ( has_post_thumbnail()) : ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
							<?php endif; ?>

					<?php the_content(); ?>			


				</div>

			</div>
		</div>
</section>
<?php endwhile; ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>