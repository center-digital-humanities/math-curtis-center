<?php

	require_once( 'external/sightbox-utilities.php' );


	// YOU CAN USE THIS FOR CACHE BUSTING
	$GLOBALS['asset_version'] = '1.0';

	/* ========================================================================================================================
	Theme specific settings
	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	add_theme_support( 'post-formats' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'menus' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// Enable Site Options section
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Site Settings',
			'menu_title'	=> 'Site Settings',
			'menu_slug' 	=> 'global-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	/* ========================================================================================================================
	Actions and Filters
	======================================================================================================================== */

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wlwmanifest_link');

	add_action('init', array('Sightbox_Utilities', 'wp_menus'));
	add_action('wp_enqueue_scripts', 'sightbox_script_enqueuer');
	add_action('wp_footer', array('Sightbox_Utilities', 'disable_embed'));
	add_action('pre_ping', array('Sightbox_Utilities', 'disable_pingback'));
	add_action('init', array('Sightbox_Utilities', 'stop_heartbeat'));
	add_action('wp_enqueue_scripts', array('Sightbox_Utilities', 'wpdocs_dequeue_dashicon'));

	add_filter('body_class', array('Sightbox_Utilities', 'add_slug_to_body_class'));
	//add_filter('show_admin_bar', '__return_true');
	add_filter('xmlrpc_enabled', '__return_false');
	add_filter('wpcf7_load_js', '__return_false');
	add_filter('wpcf7_load_css', '__return_false');


	/* ========================================================================================================================
	Scripts
	======================================================================================================================== */

	function sightbox_script_enqueuer() {
		wp_register_style( 'lityCSS', get_stylesheet_directory_uri().'/static/vendors/lity/lity.min.css', array(), null, false );
		wp_register_style( 'slickCSS', get_stylesheet_directory_uri().'/static/vendors/slick/slick.css', array(), null, false );
		wp_register_style( 'slickCSStheme', get_stylesheet_directory_uri().'/static/vendors/slick/slick-theme.css', array(), null, false );
		wp_register_style( 'screen', get_stylesheet_directory_uri().'/static/css/main.css', array(), $GLOBALS['asset_version'], false );
		
		wp_deregister_script('jquery');
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true );
		wp_register_script( 'lityJS', get_template_directory_uri().'/static/vendors/lity/lity.min.js', array( 'jquery' ), null, true );
		wp_register_script( 'slickJS', get_template_directory_uri().'/static/vendors/slick/slick.min.js', array( 'jquery' ), null, true );
		wp_register_script( 'siteJS', get_template_directory_uri().'/static/js/main.js', array( 'jquery' ), $GLOBALS['asset_version'], true );

		wp_enqueue_style( 'lityCSS' );
		wp_enqueue_style( 'slickCSS' );
		wp_enqueue_style( 'slickCSStheme' );
		wp_enqueue_style( 'screen' );

		wp_enqueue_script('jquery');
		wp_enqueue_script('lityJS');
		wp_enqueue_script('slickJS');
		wp_enqueue_script( 'siteJS' );
	}

	if ( function_exists('register_sidebar') )
   			register_sidebar(array('name' => 'Main Sidebar',
			'before_widget' => '',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
			'after_widget' => ''
		));

function wpb_list_child_pages() { 
	global $post; 
	if ( is_page() && $post->post_parent )
	    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	else
	    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
	if ( $childpages ) {
	    $string = '<ul>' . $childpages . '</ul>';
	}
	return $string;
}

function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');
	

add_filter( 'comment_form_defaults', 'rich_text_comment_form' );
function rich_text_comment_form( $args ) {
	ob_start();
	wp_editor( '', 'comment', array(
		'media_buttons' => false, // show insert/upload button(s) to users with permission
		'textarea_rows' => '10', // re-size text area
		'dfw' => false, // replace the default full screen with DFW (WordPress 3.4+)
		// 'tinymce' => array(
  //       	'theme_advanced_buttons1' => 'bold,italic,underline,strikethrough,bullist,numlist,code,blockquote,link,unlink,outdent,indent,|,undo,redo',
	 //        'theme_advanced_buttons2' => '', // 2nd row, if needed
  //       	'theme_advanced_buttons3' => '', // 3rd row, if needed
  //       	'theme_advanced_buttons4' => '' // 4th row, if needed
  // 	  	),
		'quicktags' => array(
 	       'buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,close'
	    )
	) );
	$args['comment_field'] = ob_get_clean();
	return $args;
}

// Modify ACF Form Label for Post Title Field
function wd_post_title_acf_name( $field ) {
     $field['instructions'] = 'Please use the following naming convention: Name - Draft #';
     return $field;
}
add_filter('acf/load_field/name=_post_title', 'wd_post_title_acf_name');

add_action( 'wp_enqueue_scripts', '__THEME_PREFIX__scripts' );
function __THEME_PREFIX__scripts() {
	wp_enqueue_script('jquery');
}
add_filter( 'comment_reply_link', '__THEME_PREFIX__comment_reply_link' );
function __THEME_PREFIX__comment_reply_link($link) {
	return str_replace( 'onclick=', 'data-onclick=', $link );
}
add_action( 'wp_head', '__THEME_PREFIX__wp_head' );
function __THEME_PREFIX__wp_head() {
?>
<script type="text/javascript">
	jQuery(function($){
		$('.comment-reply-link').click(function(e){
			e.preventDefault();
			var args = $(this).data('onclick');
			args = args.replace(/.*(|)/gi, '').replace(/"|s+/g, '');
			args = args.split(',');
			tinymce.EditorManager.execCommand('mceRemoveControl', true, 'comment');
			addComment.moveForm.apply( addComment, args );
			tinymce.EditorManager.execCommand('mceAddControl', true, 'comment');
		});
	});
</script>
<?php
}
?>